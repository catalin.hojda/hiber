package com.intern.exceptions;

public class NoInputFilesException extends Exception {

    public NoInputFilesException(String message) {
        super(message);
    }
}
