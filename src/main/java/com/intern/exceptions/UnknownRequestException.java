package com.intern.exceptions;

public class UnknownRequestException extends Exception {

    public UnknownRequestException(String message) {
        super(message);
    }
}
