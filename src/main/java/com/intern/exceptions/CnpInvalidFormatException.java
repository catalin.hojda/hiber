package com.intern.exceptions;

public class CnpInvalidFormatException extends Exception {

    public CnpInvalidFormatException(String message) {
        super(message);
    }
}
