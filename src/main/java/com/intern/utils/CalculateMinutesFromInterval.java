package com.intern.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class CalculateMinutesFromInterval {
    public static long getOverlapMinutes(Date startDate, Date endDate, LocalTime startHourForInterval, LocalTime endHourForInterval) throws ParseException {
        Calendar calendar = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();
        calendar.setTime(startDate);
        calendar2.setTime(endDate);
        long calculatedMinutes = 0;
        if (calendar.after(calendar2) || startHourForInterval.isAfter(endHourForInterval)) {
            System.out.println("start date > end date or start hour > end hour");
        } else {
            int year1 = calendar.get(Calendar.YEAR);
            int year2 = calendar2.get(Calendar.YEAR);
            int month1 = calendar.get(Calendar.MONTH) + 1;
            int month2 = calendar2.get(Calendar.MONTH) + 1;
            int day1 = calendar.get(Calendar.DAY_OF_MONTH);
            int day2 = calendar2.get(Calendar.DAY_OF_MONTH);
            int hourFromStartDate = calendar.get(Calendar.HOUR_OF_DAY);
            int hourFromEndDate = calendar2.get(Calendar.HOUR_OF_DAY);
            int minutesFromStartDate = calendar.get(Calendar.MINUTE);
            int minutesFromEndDate = calendar2.get(Calendar.MINUTE);
            SimpleDateFormat localDateFormat = new SimpleDateFormat("HH:mm");
            String timeOfStartDate = localDateFormat.format(startDate);
            String timeOfEndDate = localDateFormat.format(endDate);
            Date startDateWithoutTime = DateUtil.dateFormat(year1 + "-" + month1 + "-" + day1);
            Date endDateWithoutTime = DateUtil.dateFormat(year2 + "-" + month2 + "-" + day2);
            long diffInMillies = Math.abs(endDate.getTime() - startDate.getTime());
            long differenceInDays = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
            boolean alreadyCalculatedForUpperLimit = false;
            boolean daysDiffAlreadyCalculated = false;
            if (differenceInDays > 0) {
                calculatedMinutes += Math.abs(Duration.between(startHourForInterval, endHourForInterval).toMinutes()) * differenceInDays;
                daysDiffAlreadyCalculated = true;
            }
            //if dates are in the same day
            boolean isTimeOfStartDateInInterval = timeOfStartDate.compareTo(startHourForInterval.toString()) >= 0 && timeOfStartDate.compareTo(endHourForInterval.toString()) <= 0;
            boolean isTimeOfEndDateInInterval = timeOfEndDate.compareTo(startHourForInterval.toString()) >= 0 && timeOfEndDate.compareTo(endHourForInterval.toString()) <= 0;
            if (startDateWithoutTime.compareTo(endDateWithoutTime) == 0) {
                //if both dates are in interval
                if (isTimeOfStartDateInInterval && isTimeOfEndDateInInterval) {
                    calculatedMinutes += Math.abs(Duration.between(LocalTime.of(hourFromStartDate, minutesFromStartDate), LocalTime.of(hourFromEndDate, minutesFromEndDate)).toMinutes());
                }
                //if first date is in interval and the second one not
                if (isTimeOfStartDateInInterval && timeOfEndDate.compareTo(endHourForInterval.toString()) > 0) {
                    calculatedMinutes += Math.abs(Duration.between(LocalTime.of(hourFromStartDate, minutesFromStartDate), endHourForInterval).toMinutes());
                }
                //if first isn't in interval and the second one is
                if (timeOfStartDate.compareTo(startHourForInterval.toString()) < 0 && isTimeOfEndDateInInterval) {
                    calculatedMinutes += Math.abs(Duration.between(startHourForInterval, LocalTime.of(hourFromEndDate, minutesFromEndDate)).toMinutes());
                }
                //if both are not in interval
                if (timeOfStartDate.compareTo(startHourForInterval.toString()) < 0 && timeOfEndDate.compareTo(endHourForInterval.toString()) > 0) {
                    calculatedMinutes += Math.abs(Duration.between(startHourForInterval, endHourForInterval).toMinutes());
                }
            } else {
                if (!timeOfStartDate.equals(timeOfEndDate)) {
                    if (timeOfStartDate.compareTo(startHourForInterval.toString()) < 0) {
                        if (isTimeOfEndDateInInterval) {
                            calculatedMinutes += Math.abs(Duration.between(LocalTime.of(hourFromEndDate, minutesFromEndDate), startHourForInterval).toMinutes());
                            alreadyCalculatedForUpperLimit = true;
                        } else {
                            if (timeOfEndDate.compareTo(endHourForInterval.toString()) > 0) {
                                calculatedMinutes += Math.abs(Duration.between(endHourForInterval, startHourForInterval).toMinutes());
                                alreadyCalculatedForUpperLimit = true;
                            }
                        }
                    }
                    if (isTimeOfStartDateInInterval) {
                        if (differenceInDays < 1 || timeOfStartDate.compareTo(timeOfEndDate) > 0) {
                            calculatedMinutes += Math.abs(Duration.between(LocalTime.of(hourFromStartDate, minutesFromStartDate), endHourForInterval).toMinutes());
                        } else if (timeOfEndDate.compareTo(startHourForInterval.toString()) < 0) {
                            calculatedMinutes += Math.abs(Duration.between(startHourForInterval, endHourForInterval).toMinutes());
                        }
                    }
                    if (timeOfEndDate.compareTo(endHourForInterval.toString()) > 0) {
                        if (isTimeOfStartDateInInterval) {
                            calculatedMinutes += Math.abs(Duration.between(endHourForInterval, LocalTime.of(hourFromStartDate, minutesFromStartDate)).toMinutes());
                        } else {
                            if (!alreadyCalculatedForUpperLimit && timeOfStartDate.compareTo(startHourForInterval.toString()) < 0) {
                                calculatedMinutes += Math.abs(Duration.between(startHourForInterval, endHourForInterval).toMinutes());
                            } else if (timeOfStartDate.compareTo(startHourForInterval.toString()) > 0 && timeOfEndDate.compareTo(timeOfStartDate) < 0) {
                                calculatedMinutes += Math.abs(Duration.between(startHourForInterval, endHourForInterval).toMinutes());
                            }
                        }
                    }
                    if (isTimeOfEndDateInInterval) {
                        if (isTimeOfStartDateInInterval && timeOfEndDate.compareTo(timeOfStartDate) > 0) {
                            calculatedMinutes += Math.abs(Duration.between(LocalTime.of(hourFromEndDate, minutesFromEndDate), LocalTime.of(hourFromStartDate, minutesFromStartDate)).toMinutes());
                        } else {
                            if (!alreadyCalculatedForUpperLimit) {
                                calculatedMinutes += Math.abs(Duration.between(LocalTime.of(hourFromEndDate, minutesFromEndDate), startHourForInterval).toMinutes());
                            }
                        }
                    }
                }
            }
        }

        return calculatedMinutes;
    }
}