package com.intern.utils;

public class CustomStringTokenizer {

    private String string;
    private String delimiter;
    private String[] tokens;

    public CustomStringTokenizer(String string, String delimiter) {
        this.string = string;
        this.delimiter = delimiter;
        tokens = string.split(delimiter);
    }

    public void setString(String string) {
        this.string = string;
    }

    public void setTokens(String[] tokens) {
        this.tokens = tokens;
    }

    public void setDelimiter(String delimiter) {
        this.delimiter = delimiter;
    }

    public void getTokens() {
        for (String s : tokens) {
            System.out.println(s);
        }
    }

    public int getTokensLength() {
        return tokens.length;
    }

    public String getTokenAt(int index) {
        return tokens[index];
    }
}
