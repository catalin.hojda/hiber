package com.intern.utils;

import java.util.Calendar;
import java.util.Random;

public class GenerateValidCnps {
    public static void generateValidCnps() {
        int numberOfCnps = 1 + (int) (Math.random() * 100);
        int[] firstDigitFromCnp = {1, 2, 5, 6};
        for (int i = 0; i < numberOfCnps; i++) {
            int firstDigit = firstDigitFromCnp[(int) (Math.random() * 3)];
            int year = (int) (Math.random() * 100);
            int month = (int) (Math.random() * 12 + 1);
            int day = -1;
            String monthAsString = month + "";
            String yearAsString = year + "";

            if (year < 10) {
                yearAsString = "0" + yearAsString;
            }
            if (month < 10) {
                monthAsString = "0" + month;
            }
            if (firstDigit != 1 && firstDigit != 2) {
                //if first digit = 5 or 6 it means that the year is possible to be only <20
                year = (int) (Math.random() * 21);
                yearAsString = year + "";
                if (year < 10) {
                    yearAsString = "0" + year;
                }
            }
            String first3DigitsFromCnp = firstDigit + yearAsString;
            int yearOfBirth = calculateYear(first3DigitsFromCnp);
            boolean isLeapYear = isLeapYear(yearOfBirth);
            day = returnCorrectDayForLeapYear(monthAsString, isLeapYear);
            String dayAsString = day + "";
            if (day < 10) {
                dayAsString = "0" + dayAsString;
            }
            String stringOf6Digits = getRandomNumberString();
            String cnp = firstDigit + yearAsString + monthAsString + dayAsString + stringOf6Digits;
            System.out.println(cnp);
        }
    }

    public static int calculateYear(String first3DigitsFromCnp) {
        char firstDigit = first3DigitsFromCnp.charAt(0);
        String year = first3DigitsFromCnp.substring(1, 3);
        String yearOfBirth = null;
        if (firstDigit == '5' || firstDigit == '6') {
            yearOfBirth = "20" + year;
        } else {
            if (firstDigit == '1' || firstDigit == '2') {
                yearOfBirth = "19" + year;
            }
        }
        assert yearOfBirth != null;
        return Integer.parseInt(yearOfBirth);
    }

    public static boolean isLeapYear(int year) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        return cal.getActualMaximum(Calendar.DAY_OF_YEAR) > 365;
    }

    public static int calculateDayForSpecificMonth(int month) {
        int day;
        if (month % 2 == 0) {
            day = (int) (Math.random() * 31 + 1);
        } else {
            day = (int) (Math.random() * 30 + 1);
        }
        return day;
    }

    public static int returnCorrectDayForLeapYear(String monthAsString, boolean isLeapYear) {
        int day;
        int month = Integer.parseInt(monthAsString);
        if (isLeapYear) {
            if ("02".equals(monthAsString)) {
                day = (int) (Math.random() * 29 + 1);
            } else {
                day = calculateDayForSpecificMonth(month);
            }
        } else {
            if ("02".equals(monthAsString)) {
                day = (int) (Math.random() * 28 + 1);
            } else {
                day = calculateDayForSpecificMonth(month);
            }
        }
        return day;
    }

    public static String getRandomNumberString() {
        // It will generate 6 digit random Number.
        // from 0 to 999999
        Random rnd = new Random();
        int number = rnd.nextInt(999999);

        // this will convert any number sequence into 6 character.
        return String.format("%06d", number);
    }
}
