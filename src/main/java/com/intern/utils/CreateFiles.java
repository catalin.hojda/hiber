package com.intern.utils;

import java.io.File;
import java.io.IOException;

public class CreateFiles {
    public static File getOutputDirectoryAndCreateOutputFiles() throws IOException {
        String path = "D:\\SEI\\output\\";
        String[] fileNames = {"women.txt", "men.txt", "children.txt"};
        File directory = new File(path);
        if (!directory.exists()) {
            directory.mkdir();
        }
        for (String fileName : fileNames) {
            File file = new File(path + fileName);
            file.createNewFile();
        }
        return directory;
    }
}
