package com.intern.utils;

import com.intern.exceptions.CnpInvalidFormatException;
import com.intern.exceptions.UnknownRequestException;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class Validate {

    public static boolean checkNames(String firstName1, String firstName2, String lastName) {

        return isAlpha(firstName1) && isAlpha(firstName2) && isAlpha(lastName);
    }

    public static boolean checkCnp(String cnp) throws CnpInvalidFormatException {
        boolean result = false;
        if (cnp.matches("^[0-9]{13}$") && (cnp.charAt(0) == '1' || cnp.charAt(0) == '2' || cnp.charAt(0) == '5' || cnp.charAt(0) == '6')) {
            result = true;
        } else {
            throw new CnpInvalidFormatException("The given string as parameter is not a valid CNP : " + cnp);
        }
        return result;
    }

    public static boolean checkEmail(String firstName1, String firstName2, String lastName, String email) {

        String s = firstName1 + "." + firstName2 + "." + lastName;
        return email.matches("^" + s + "+@+\\w+\\.+com");
    }

    public static boolean isAlpha(String name) {
        char[] chars = name.toCharArray();

        for (char c : chars) {
            if (!Character.isLetter(c)) {
                return false;
            }
        }
        return true;
    }

    public static boolean isAdult(String cnp) {
        char firstDigit = cnp.charAt(0);
        String year = cnp.substring(1, 3);
        String month = cnp.substring(3, 5);
        int compareYear = year.compareTo("02");
        int compareMonth = month.compareTo("08");
        return ((firstDigit == '5' || firstDigit == '6') && (compareYear < 0)) ||
                (compareYear == 0 && compareMonth < 0) ||
                (firstDigit == '1' || firstDigit == '2');
    }

    public static int calculateAge(String cnp) {
        char firstDigit = cnp.charAt(0);
        String year = cnp.substring(1, 3);
        String yearOfBirth = null;
        int yearOfBirthInteger = 0;
        if (firstDigit == '5' || firstDigit == '6') {
            yearOfBirth = "20" + year;
        } else {
            if (firstDigit == '1' || firstDigit == '2') {
                yearOfBirth = "19" + year;
            }
        }
        assert yearOfBirth != null;
        try {
            yearOfBirthInteger = Integer.parseInt(yearOfBirth);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        return currentYear - yearOfBirthInteger;
    }

    public static String getGender(String cnp) {
        String gender = "";
        char firstDigit = cnp.charAt(0);
        if (firstDigit == '1' || firstDigit == '5') {
            gender = "man";
        } else {
            if (firstDigit == '2' || firstDigit == '6')
                gender = "woman";
        }
        return gender;
    }

    public static boolean validateUrl(String urlLastParameter) throws UnknownRequestException {
        boolean isValid = false;
        List<String> parameterOptions = Arrays.asList("adultUsers", "usersFilteredGenderAndAge", "generateOutputFiles");
        if (parameterOptions.contains(urlLastParameter)) {
            isValid = true;
        } else {
            throw new UnknownRequestException("invalid url");
        }
        return isValid;
    }
}
