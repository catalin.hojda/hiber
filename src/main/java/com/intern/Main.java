package com.intern;

import com.intern.model.AuditLog;
import com.intern.model.Holiday;
import com.intern.model.PublicHoliday;
import com.intern.model.User;
import com.intern.utils.DateUtil;
import org.hibernate.*;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import java.io.*;
import java.text.ParseException;
import java.time.LocalTime;
import java.util.Date;

import static com.intern.utils.CalculateMinutesFromInterval.getOverlapMinutes;

public class Main {

    final static File folder = new File("D:\\SEI\\input\\");

    private static final Configuration configuration = new Configuration().addAnnotatedClass(Holiday.class).addAnnotatedClass(User.class).addAnnotatedClass(AuditLog.class).addAnnotatedClass(PublicHoliday.class).configure();
    private static final StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
    private static final SessionFactory factory = configuration.buildSessionFactory(builder.build());

    public static void main(String[] args) throws ParseException {

        String startDateAsString = "2020-09-02 15:00";
        String endDateAsString = "2020-09-04 12:30";
        LocalTime lowerLimitHourInterval = LocalTime.of(8, 0);
        LocalTime upperLimitHourInterval = LocalTime.of(15, 0);
        Date startDate = DateUtil.dateFormatWithHourAndMin(startDateAsString);
        Date endDate = DateUtil.dateFormatWithHourAndMin(endDateAsString);
        System.out.println(getOverlapMinutes(startDate, endDate, lowerLimitHourInterval, upperLimitHourInterval) + " minutes");
    }
}

