package com.intern.service;

import com.intern.datastructures.CustomDoublyLinkedList;
import com.intern.datastructures.Node;
import com.intern.exceptions.CnpInvalidFormatException;
import com.intern.exceptions.NoInputFilesException;
import com.intern.model.User;
import com.intern.utils.CustomStringTokenizer;
import com.intern.utils.Validate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import javax.persistence.Query;
import java.io.*;
import java.util.HashSet;
import java.util.List;

public class FileService {
    private final Configuration configuration;
    private final StandardServiceRegistryBuilder builder;
    private final SessionFactory factory;
    private final UserService userService;

    public FileService(Configuration configuration, StandardServiceRegistryBuilder builder, SessionFactory factory) {
        this.configuration = configuration;
        this.builder = builder;
        this.factory = factory;
        userService = new UserService(configuration, builder, factory);
    }

    public HashSet<User> parseFile(File file) throws IOException {

        FileInputStream fstream = new FileInputStream(file);
        BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
        CustomStringTokenizer stringTokenizer;
        String line;
        HashSet<User> persons = new HashSet<>();
        List<User> results = null;
        while ((line = br.readLine()) != null) {
            stringTokenizer = new CustomStringTokenizer(line, "#");
            System.out.println("reading " + stringTokenizer.getTokensLength() + " persons from " + file.getName());
            int i = 0;
            while (i < stringTokenizer.getTokensLength()) {
                String s = stringTokenizer.getTokenAt(i);
                String[] splitOut = s.split("\\|");
                String firstName1 = splitOut[0];
                String firstName2 = splitOut[1];
                String lastName = splitOut[2];
                String cnp = splitOut[3];
                String email = splitOut[4];
                String name = lastName + " " + firstName1 + " " + firstName2;
                try {
                    if (Validate.checkNames(firstName1, firstName2, lastName) && Validate.checkCnp(cnp) && Validate.checkEmail(firstName1, firstName2, lastName, email)) {
                        Session session = factory.openSession();
                        Query query = session.createNamedQuery("User_getUserByEmail", User.class);
                        query.setParameter("userEmail", email);
                        results = query.getResultList();
                        if (results.isEmpty()) {
                            userService.createUser(email, name, "", cnp, null, "", -1, 21, -1);
                        } else {
                            userService.updateUser(results.get(0).getId(), email, name, "", cnp, null, "", -1, 21, -1);
                        }
                        session.close();
                    }
                } catch (CnpInvalidFormatException e) {
                    System.out.println(e.getMessage());
                } finally {
                    i++;
                }
            }
        }
        br.close();
        return persons;
    }

    public void parseAllFiles(File folder) throws IOException, NoInputFilesException {
        File[] fileNames = folder.listFiles();
        // listFiles() returns null if folder's path is not a directory
        if (fileNames != null && fileNames.length != 0) {
            for (File file : fileNames) {
                parseFile(file);
            }
        } else {
            throw new NoInputFilesException("no input files");
        }
    }

    public String buildDataForWritingInFile(CustomDoublyLinkedList users) {
        StringBuilder result = new StringBuilder();
        String delimiter = "|";
        String tokenizer = "#";
        Node iterator = users.getHead();
        while (iterator != null) {
            User user = (User) iterator.getData();
            String[] names = user.getName().split(" ");
            //names[0] in database represents lastname , in our files names[0] should be firstName1
            result.append(names[1]).append(delimiter).append(names[2]).append(delimiter).append(names[0]).append(delimiter).append(user.getCnp()).append(delimiter).append(user.getEmail()).append(tokenizer);
            iterator = iterator.getNext();
        }
        return result.toString();
    }

    public void writeInFile(File file, String value) throws IOException {
        FileWriter myWriter = null;
        try {
            myWriter = new FileWriter(file);
            myWriter.write(value);
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        } finally {
            assert myWriter != null;
            myWriter.close();
        }
    }

    public void populateFilesFromDirectory(File folder) throws IOException {
        File[] fileNames = folder.listFiles();
        if (fileNames != null) {
            for (File file : fileNames) {
                System.out.println("Writing in " + file.getName());
                if ("women.txt".equals(file.getName())) {
                    writeInFile(file, buildDataForWritingInFile(userService.getUsersByGender("woman")));
                } else if ("men.txt".equals(file.getName())) {
                    writeInFile(file, buildDataForWritingInFile(userService.getUsersByGender("man")));
                } else if ("children.txt".equals(file.getName())) {
                    writeInFile(file, buildDataForWritingInFile(userService.getUsersUnderAge(35)));
                }
            }
        }
    }
}
