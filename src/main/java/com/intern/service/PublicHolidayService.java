package com.intern.service;

import com.intern.model.PublicHoliday;
import com.intern.utils.DateUtil;
import org.hibernate.*;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Order;

import javax.persistence.TypedQuery;
import java.awt.*;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

public class PublicHolidayService {

    private final Configuration configuration;
    private final StandardServiceRegistryBuilder builder;
    private final SessionFactory factory;

    public PublicHolidayService(Configuration configuration, StandardServiceRegistryBuilder builder, SessionFactory factory) {
        this.configuration = configuration;
        this.builder = builder;
        this.factory = factory;
    }

    public PublicHoliday createPublicHoliday(Date day, String description) {
        Session session = factory.openSession();
        Transaction tx = null;
        PublicHoliday publicHoliday = null;
        try {
            tx = session.beginTransaction();
            publicHoliday = new PublicHoliday(day, description);
            session.save(publicHoliday);
            tx.commit();
        } catch (HeadlessException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return publicHoliday;
    }

    public void updatePublicHoliday(int id, Date day, String description) {
        Session session = factory.openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            PublicHoliday publicHoliday = session.get(PublicHoliday.class, id);
            if (publicHoliday != null) {
                publicHoliday.setDay(day);
                publicHoliday.setDescription(description);
                session.update(publicHoliday);
            } else {
                System.out.println("Public holiday with id= " + id + " does not exist !");
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public PublicHoliday getPublicHolidayById(int id) {

        Session session = factory.openSession();
        Transaction tx = null;
        PublicHoliday publicHoliday = null;
        try {
            tx = session.beginTransaction();
            publicHoliday = session.get(PublicHoliday.class, id);
            if (publicHoliday != null) {
                System.out.println(publicHoliday);
            } else {
                System.out.println("Public holiday with id= " + id + " does not exist !");
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return publicHoliday;
    }

    public List<PublicHoliday> getAllPublicHolidays() {
        Session session = factory.openSession();
        Transaction tx = null;
        List<PublicHoliday> publicHolidays = null;
        try {
            tx = session.beginTransaction();
            Criteria cr = session.createCriteria(PublicHoliday.class);
            cr.addOrder(Order.asc("id"));
            publicHolidays = cr.list();

            for (Object o : publicHolidays) {
                PublicHoliday pbHoliday = (PublicHoliday) o;
                System.out.println(pbHoliday);
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return publicHolidays;
    }

    public void deletePublicHolidayById(int id) {
        Session session = factory.openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            PublicHoliday publicHoliday = session.get(PublicHoliday.class, id);
            if (publicHoliday != null) {
                session.delete(publicHoliday);
            } else {
                System.out.println("Public holiday with id= " + id + " does not exist !");
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public List<PublicHoliday> getPublicHolidaysAfter(String stringDate) {
        Session session = factory.openSession();
        Transaction tx = null;
        List<PublicHoliday> returnedPublicHolidays = null;

        try {
            tx = session.beginTransaction();
            TypedQuery query = session.getNamedQuery("PublicHoliday_getPublicHolidaysAfterDate");
            query.setParameter("startDay", DateUtil.dateFormat(stringDate));
            returnedPublicHolidays = query.getResultList();
            if (!returnedPublicHolidays.isEmpty()) {
                for (Object e : returnedPublicHolidays) {
                    System.out.println(e);
                }
            } else {
                System.out.println("There are no public holidays after " + stringDate);
            }

            tx.commit();
        } catch (HibernateException | ParseException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return returnedPublicHolidays;
    }


}