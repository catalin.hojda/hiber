package com.intern.service;

import com.intern.model.Holiday;
import com.intern.model.User;
import org.hibernate.*;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Order;

import javax.persistence.TypedQuery;
import java.awt.*;
import java.util.Date;
import java.util.List;


public class HolidayService {

    private final Configuration configuration;
    private final StandardServiceRegistryBuilder builder;
    private final SessionFactory factory;

    public HolidayService(Configuration configuration, StandardServiceRegistryBuilder builder, SessionFactory factory) {
        this.configuration = configuration;
        this.builder = builder;
        this.factory = factory;
    }

    public Holiday createHoliday(int requester_id, Date start_date, Date end_date, String type, String status, Date approved_time) {
        Session session = factory.openSession();
        Transaction tx = null;
        Holiday holiday = null;
        try {
            tx = session.beginTransaction();
            holiday = new Holiday(requester_id, start_date, end_date, type, status, approved_time);
            session.save(holiday);
            tx.commit();
        } catch (HeadlessException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return holiday;
    }

    public void updateHoliday(int id, int requester_id, Date start_date, Date end_date, String type, String status, Date approved_time, User user) {
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Holiday updateHoliday = session.get(Holiday.class, id);
            if (updateHoliday != null) {
                updateHoliday.setRequester_id(requester_id);
                updateHoliday.setStart_date(start_date);
                updateHoliday.setEnd_date(end_date);
                updateHoliday.setType(type);
                updateHoliday.setStatus(status);
                updateHoliday.setApproved_time(approved_time);
                updateHoliday.setUser(user);
                session.update(updateHoliday);
            } else {
                System.out.println("Holiday with id= " + id + " does not exist !");
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public Holiday getHolidayById(int id) {
        Session session = factory.openSession();
        Transaction tx = null;
        Holiday returnedHoliday = null;
        try {
            tx = session.beginTransaction();
            returnedHoliday = session.get(Holiday.class, id);
            if (returnedHoliday != null) {
                System.out.println(returnedHoliday);
            } else {
                System.out.println("Holiday with id= " + id + " does not exist !");
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return returnedHoliday;
    }


    public List<Holiday> getAllHolidays() {
        Session session = factory.openSession();
        Transaction tx = null;
        List<Holiday> returnedHolidays = null;
        try {
            tx = session.beginTransaction();
            Criteria cr = session.createCriteria(Holiday.class);
            cr.addOrder(Order.asc("id"));
            returnedHolidays = cr.list();
            for (Object object : returnedHolidays) {
                Holiday holiday = (Holiday) object;
                System.out.println(holiday.toString());
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return returnedHolidays;
    }

    public void deleteHolidayById(int id) {
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Holiday getHoliday = session.get(Holiday.class, id);
            if (getHoliday != null) {
                session.delete(getHoliday);
            } else {
                System.out.println("Holiday with id= " + id + " does not exist !");
            }

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public List<Holiday> getHolidaysByUser(int userId) {
        Session session = factory.openSession();
        Transaction tx = null;
        List<Holiday> returnedHolidays = null;
        try {
            tx = session.beginTransaction();
            TypedQuery query = session.getNamedQuery("Holiday_getHolidaysByUser");
            query.setParameter("userId", userId);
            returnedHolidays = query.getResultList();
            if (!returnedHolidays.isEmpty()) {
                for (Object e : returnedHolidays) {
                    System.out.println(e);
                }
            } else {
                System.out.println("User with id = " + userId + " does not have any Holidays");
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return returnedHolidays;
    }


}
