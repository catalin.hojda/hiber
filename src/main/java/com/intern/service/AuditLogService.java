package com.intern.service;

import com.intern.datastructures.CustomDoublyLinkedList;
import com.intern.datastructures.Node;
import com.intern.model.AuditLog;
import org.hibernate.*;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Order;

import javax.persistence.TypedQuery;
import java.awt.*;
import java.util.*;
import java.util.List;

public class AuditLogService {

    private final Configuration configuration;
    private final StandardServiceRegistryBuilder builder;
    private final SessionFactory factory;

    public AuditLogService(Configuration configuration, StandardServiceRegistryBuilder builder, SessionFactory factory) {
        this.configuration = configuration;
        this.builder = builder;
        this.factory = factory;
    }

    public AuditLog createAuditLog(String action_type, Date action_time, String initiator_email) {
        Session session = factory.openSession();
        Transaction tx = null;
        AuditLog auditLog = null;
        try {
            tx = session.beginTransaction();
            auditLog = new AuditLog(action_type, action_time, initiator_email);
            session.save(auditLog);
            tx.commit();
        } catch (HeadlessException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return auditLog;
    }

    public void updateAuditLog(int id, String action_type, Date action_time, String email) {
        Session session = factory.openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            AuditLog auditLog = session.get(AuditLog.class, id);
            if (auditLog != null) {
                auditLog.setAction_type(action_type);
                auditLog.setAction_time(action_time);
                auditLog.setInitiator_email(email);
                session.update(auditLog);
            } else {
                System.out.println("Log with id= " + id + " does not exist !");
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public AuditLog getAuditLogById(int id) {

        Session session = factory.openSession();
        Transaction tx = null;
        AuditLog auditLog = null;
        try {
            tx = session.beginTransaction();
            auditLog = session.get(AuditLog.class, id);
            if (auditLog != null) {
                System.out.println(auditLog);
            } else {
                System.out.println("Log with id= " + id + " does not exist !");
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return auditLog;
    }

    public CustomDoublyLinkedList getAllAuditLogs() {
        Session session = factory.openSession();
        Transaction tx = null;
        List<AuditLog> auditLogs = null;
        CustomDoublyLinkedList customDoublyLinkedList = new CustomDoublyLinkedList();
        try {
            tx = session.beginTransaction();
            Criteria cr = session.createCriteria(AuditLog.class);
            cr.addOrder(Order.asc("id"));
            auditLogs = cr.list();
            for (Object object : auditLogs) {
                customDoublyLinkedList.insertFirst(object);
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return customDoublyLinkedList;
    }

    public void deleteAuditLogById(int id) {
        Session session = factory.openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            AuditLog auditLog = session.get(AuditLog.class, id);
            if (auditLog != null) {
                session.delete(auditLog);
            } else {
                System.out.println("Log with id= " + id + " does not exist !");
            }

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public CustomDoublyLinkedList getAuditLogsByUserEmail(String email) {
        Session session = factory.openSession();
        Transaction tx = null;
        List<AuditLog> returnedLogs = null;
        CustomDoublyLinkedList customDoublyLinkedList = new CustomDoublyLinkedList();
        try {
            tx = session.beginTransaction();
            TypedQuery query = session.getNamedQuery("AuditLog_getLogsByUserEmail");
            query.setParameter("userEmail", email);
            returnedLogs = query.getResultList();
            if (!returnedLogs.isEmpty()) {
                for (Object e : returnedLogs) {
                    customDoublyLinkedList.insertFirst(e);
                }
            } else {
                System.out.println("User with email = " + email + " does not have any logs");
            }

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return customDoublyLinkedList;
    }

    public Map<Integer, List<AuditLog>> getAuditLogsGroupedByDay() {
        HashMap<Integer, List<AuditLog>> auditLogsGroupedByDay = new HashMap<>();
        CustomDoublyLinkedList allAuditLogs = getAllAuditLogs();
        Node currentNode = allAuditLogs.getHead();
        while (currentNode != null) {
            AuditLog auditLog = (AuditLog) currentNode.getData();
            String date = auditLog.getAction_time().toString();
            Integer day = Integer.parseInt(date.substring(date.lastIndexOf("-") + 1));
            if (auditLogsGroupedByDay.containsKey(day)) {
                auditLogsGroupedByDay.get(day).add(auditLog);
            } else {
                List<AuditLog> auditLogsFromSameDay = new ArrayList<>();
                auditLogsFromSameDay.add(auditLog);
                auditLogsGroupedByDay.put(day, auditLogsFromSameDay);
            }
            currentNode = currentNode.getNext();
        }
        return auditLogsGroupedByDay;
    }
}