package com.intern.service;

import com.intern.datastructures.CustomDoublyLinkedList;
import com.intern.datastructures.Node;
import com.intern.model.User;
import com.intern.utils.Validate;
import org.hibernate.*;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Order;

import javax.persistence.TypedQuery;
import java.util.Date;
import java.util.List;

public class UserService {

    private final Configuration configuration;
    private final StandardServiceRegistryBuilder builder;
    private final SessionFactory factory;

    public UserService(Configuration configuration, StandardServiceRegistryBuilder builder, SessionFactory factory) {
        this.configuration = configuration;
        this.builder = builder;
        this.factory = factory;
    }

    public User createUser(String email, String name, String phone, String cnp, Date employment_date, String role, int manager_id, int days_per_year, int current_year_remaining_days) {
        Session session = factory.openSession();
        Transaction tx = null;
        User user = null;

        try {
            tx = session.beginTransaction();
            user = new User(email, name, phone, cnp, employment_date, role, manager_id, days_per_year, current_year_remaining_days);
            session.save(user);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return user;

    }

    public void updateUser(int id, String email, String name, String phone, String cnp, Date employment_date, String role, int manager_id, int days_per_year, int current_year_remaining_days) {
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            User updateUser = session.get(User.class, id);
            if (updateUser != null) {

                updateUser.setEmail(email);
                updateUser.setName(name);
                updateUser.setPhone(phone);
                updateUser.setCnp(cnp);
                updateUser.setEmployment_date(employment_date);
                updateUser.setRole(role);
                updateUser.setManager_id(manager_id);
                updateUser.setDays_per_year(days_per_year);
                updateUser.setCurrent_year_remaining_days(current_year_remaining_days);
                session.update(updateUser);

            } else {
                System.out.println("User with id= " + id + " does not exist !");
            }

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public User getUserById(int id) {
        Session session = factory.openSession();
        Transaction tx = null;
        User returnedUser = null;
        try {
            tx = session.beginTransaction();
            returnedUser = session.get(User.class, id);
            if (returnedUser != null) {
                System.out.println(" " + returnedUser);

            } else {
                System.out.println("User with id= " + id + " does not exist !");
            }

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }

        return returnedUser;
    }

    public CustomDoublyLinkedList getAllUsers() {
        Session session = factory.openSession();
        Transaction tx = null;
        List<User> users = null;
        CustomDoublyLinkedList customDoublyLinkedList = new CustomDoublyLinkedList();
        try {
            tx = session.beginTransaction();
            Criteria cr = session.createCriteria(User.class);
            cr.addOrder(Order.asc("id"));
            users = cr.list();
            for (User user : users) {
                customDoublyLinkedList.insertFirst(user);
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return customDoublyLinkedList;
    }

    public CustomDoublyLinkedList getAdultUsers() {
        CustomDoublyLinkedList allUsers = getAllUsers();
        CustomDoublyLinkedList adultUsers = new CustomDoublyLinkedList();
        Node iterator = allUsers.getHead();

        while (iterator != null) {
            if (Validate.isAdult(((User) iterator.getData()).getCnp())) {
                adultUsers.insertFirst(iterator);
            }
            iterator = iterator.getNext();
        }
        return adultUsers;
    }

    public CustomDoublyLinkedList getUsersByGenderAndAge(String gender, int age) {
        CustomDoublyLinkedList allUsers = getAllUsers();
        CustomDoublyLinkedList usersByGenderAndAge = new CustomDoublyLinkedList();
        Node iterator = allUsers.getHead();
        while (iterator != null) {
            User user = (User) iterator.getData();
            if (Validate.getGender(user.getCnp()).equals(gender.toLowerCase()) && Validate.calculateAge(user.getCnp()) == age) {
                usersByGenderAndAge.insertFirst(user);
            }
            iterator = iterator.getNext();
        }
        return usersByGenderAndAge;
    }

    public CustomDoublyLinkedList getUsersByGender(String gender) {
        CustomDoublyLinkedList allUsers = getAllUsers();
        CustomDoublyLinkedList resultedUsers = new CustomDoublyLinkedList();
        Node iterator = allUsers.getHead();
        while (iterator != null) {
            User user = (User) iterator.getData();
            if (Validate.getGender(user.getCnp()).equals(gender)) {
                resultedUsers.insertFirst(user);
            }
            iterator = iterator.getNext();
        }
        return resultedUsers;
    }

    public CustomDoublyLinkedList getUsersUnderAge(int age) {
        CustomDoublyLinkedList allUsers = getAllUsers();
        CustomDoublyLinkedList usersByAge = new CustomDoublyLinkedList();
        Node iterator = allUsers.getHead();
        while (iterator != null) {
            User user = (User) iterator.getData();
            if (Validate.calculateAge(user.getCnp()) < age) {
                usersByAge.insertFirst(user);
            }
            iterator = iterator.getNext();
        }
        return usersByAge;
    }

    public void deleteUserById(int id) {
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            User getUser = session.get(User.class, id);
            if (getUser != null) {
                session.delete(getUser);

            } else {
                System.out.println("User with id= " + id + " does not exist !");
            }

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public CustomDoublyLinkedList getEmployeeUsers(String role) {
        Session session = factory.openSession();
        Transaction tx = null;
        List<User> returnedUsers = null;
        CustomDoublyLinkedList employeeUsers = new CustomDoublyLinkedList();
        try {
            tx = session.beginTransaction();
            TypedQuery query = session.getNamedQuery("User_getEmployeeUsers");
            query.setParameter("userRole", role);
            returnedUsers = query.getResultList();
            if (!returnedUsers.isEmpty()) {
                for (Object e : returnedUsers) {
                    employeeUsers.insertFirst(e);
                }
            } else {
                System.out.println("There are no users with the role:  " + role);
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return employeeUsers;
    }

}
