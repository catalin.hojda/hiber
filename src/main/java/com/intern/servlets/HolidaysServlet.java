package com.intern.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.intern.model.AuditLog;
import com.intern.model.Holiday;
import com.intern.model.PublicHoliday;
import com.intern.model.User;
import com.intern.service.HolidayService;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * This servlet is used to handle GET requests for Holiday class
 * It has 2 methods for listing all holidays and list a holiday by id
 */

public class HolidaysServlet extends HttpServlet {

    private static final Configuration configuration = new Configuration().addAnnotatedClass(Holiday.class).addAnnotatedClass(User.class).addAnnotatedClass(AuditLog.class).addAnnotatedClass(PublicHoliday.class).configure();
    private static final StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
    private static final SessionFactory factory = configuration.buildSessionFactory(builder.build());
    HolidayService holidayService = new HolidayService(configuration, builder, factory);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String requestUrl = request.getRequestURI();
        String parameter = requestUrl.substring(requestUrl.lastIndexOf("/") + 1);
        int lastParameter = 0;
        if (parameter.equals("all")) {
            lastParameter = 1;
        } else if (StringUtils.isNumeric(parameter)) {
            lastParameter = 2;
        }

        switch (lastParameter) {
            case 1:
                try {
                    listAllHolidays(request, response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case 2:
                try {
                    listHolidayById(request, response, parameter);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            default:
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                break;
        }
    }

    private void listAllHolidays(HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        List<Holiday> holidays = holidayService.getAllHolidays();

        // This is used for generating the JSON response without creating it manually
        ObjectMapper objMapper = new ObjectMapper();
        if (holidays != null) {
            String output = objMapper.writeValueAsString(holidays);
            response.getOutputStream().print(output);
        } else {
            //That user wasn't found, so return an empty JSON object. We could also return an error.
            response.getOutputStream().println("{}");
        }
    }

    private void listHolidayById(HttpServletRequest request, HttpServletResponse response, String path) throws Exception {

        Holiday requestedHoliday = holidayService.getHolidayById(Integer.parseInt(path));
        // This is used for generating the JSON response without creating it manually
        ObjectMapper objMapper = new ObjectMapper();
        if (requestedHoliday != null) {
            String output = objMapper.writeValueAsString(requestedHoliday);
            response.getOutputStream().print(output);
        } else {
            //That user wasn't found, so return an empty JSON object. We could also return an error.
            response.getOutputStream().println("{}");
        }
    }
}
