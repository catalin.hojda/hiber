package com.intern.servlets;

import com.intern.datastructures.CustomDoublyLinkedList;
import com.intern.model.AuditLog;
import com.intern.model.Holiday;
import com.intern.model.PublicHoliday;
import com.intern.model.User;
import com.intern.service.AuditLogService;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This servlet is used to handle GET requests for AuditLog class
 * It has 2 methods for listing all the auditlogs and list an auditlog by id
 */

public class AuditLogsServlet extends HttpServlet {

    private static final Configuration configuration = new Configuration().addAnnotatedClass(Holiday.class).addAnnotatedClass(User.class).addAnnotatedClass(AuditLog.class).addAnnotatedClass(PublicHoliday.class).configure();
    private static final StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
    private static final SessionFactory factory = configuration.buildSessionFactory(builder.build());
    AuditLogService auditLogService = new AuditLogService(configuration, builder, factory);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String requestUrl = request.getRequestURI();
        String parameter = requestUrl.substring(requestUrl.lastIndexOf("/") + 1);
        int lastParameter = 0;
        if (parameter.equals("all")) {
            lastParameter = 1;
        } else if (StringUtils.isNumeric(parameter)) {
            lastParameter = 2;
        }
        switch (lastParameter) {
            case 1:
                try {
                    listAllAuditLogs(request, response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case 2:
                try {
                    listAuditLogById(request, response, parameter);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            default:
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                break;
        }


    }

    private void listAuditLogById(HttpServletRequest request, HttpServletResponse response, String path) throws Exception {
        AuditLog requestedAuditLog = auditLogService.getAuditLogById(Integer.parseInt(path));
        if (requestedAuditLog != null) {
            response.getOutputStream().print(requestedAuditLog.toString());
        } else {
            //That user wasn't found, so return an empty JSON object. We could also return an error.
            response.getOutputStream().println("{}");
        }
    }

    private void listAllAuditLogs(HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        CustomDoublyLinkedList auditLogs = auditLogService.getAllAuditLogs();
        if (auditLogs != null) {
            response.getOutputStream().print(auditLogs.toString());
        } else {
            //That user wasn't found, so return an empty JSON object. We could also return an error.
            response.getOutputStream().println("{}");
        }
    }
}
