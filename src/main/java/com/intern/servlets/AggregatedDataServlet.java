package com.intern.servlets;

import com.intern.datastructures.CustomDoublyLinkedList;
import com.intern.exceptions.UnknownRequestException;
import com.intern.model.AuditLog;
import com.intern.model.Holiday;
import com.intern.model.PublicHoliday;
import com.intern.model.User;
import com.intern.service.FileService;
import com.intern.service.UserService;
import com.intern.utils.CreateFiles;
import com.intern.utils.Validate;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

public class AggregatedDataServlet extends HttpServlet {
    private static final Configuration configuration = new Configuration().addAnnotatedClass(Holiday.class).addAnnotatedClass(User.class).addAnnotatedClass(AuditLog.class).addAnnotatedClass(PublicHoliday.class).configure();
    private static final StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
    private static final SessionFactory factory = configuration.buildSessionFactory(builder.build());
    UserService userService = new UserService(configuration, builder, factory);
    FileService fileService = new FileService(configuration, builder, factory);

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String genderParameter = req.getParameter("gender");
        String ageParameter = req.getParameter("age");
        String requestUrl = req.getRequestURI();
        String parameter = requestUrl.substring(requestUrl.lastIndexOf("/") + 1);
        try {
            if (Validate.validateUrl(parameter)) {
                switch (parameter) {
                    case "adultUsers":
                        try {
                            listAdultUsers(req, resp);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case "usersFilteredGenderAndAge":
                        try {
                            int ageParam = Integer.parseInt(ageParameter);
                            listUsersByAgeAndGender(req, resp, genderParameter, ageParam);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case "generateOutputFiles":
                        File outputDirectory = CreateFiles.getOutputDirectoryAndCreateOutputFiles();
                        fileService.populateFilesFromDirectory(outputDirectory);
                        resp.getWriter().print("Wrote in files successfully");
                        break;
//                default:
//                    resp.sendError(HttpServletResponse.SC_NOT_FOUND);
//                    break;
                }
            }
        } catch (UnknownRequestException e) {
            resp.getOutputStream().println("{ " + e.getMessage() + " }");
        }
    }

    private void listAdultUsers(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CustomDoublyLinkedList users = userService.getAdultUsers();

        if (!users.isEmpty()) {
            response.getOutputStream().print(users.toString());
        } else {
            //That user wasn't found, so return an empty JSON object. We could also return an error.
            response.getOutputStream().println("{}");
        }
    }

    private void listUsersByAgeAndGender(HttpServletRequest request, HttpServletResponse response, String gender, int age) throws IOException {
        CustomDoublyLinkedList users = userService.getUsersByGenderAndAge(gender, age);

        if (!users.isEmpty()) {
            response.getOutputStream().print(users.toString());
        } else {
            //That user wasn't found, so return an empty JSON object. We could also return an error.
            response.getOutputStream().println("{}");
        }
    }
}
