package com.intern.servlets;

import com.intern.datastructures.CustomDoublyLinkedList;
import com.intern.model.*;
import com.intern.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This servlet is used to handle GET requests for User class
 * It has 2 methods for listing all the users and list a user by id
 */

public class UsersServlet extends HttpServlet {

    private static final Configuration configuration = new Configuration().addAnnotatedClass(Holiday.class).addAnnotatedClass(User.class).addAnnotatedClass(AuditLog.class).addAnnotatedClass(PublicHoliday.class).configure();
    private static final StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
    private static final SessionFactory factory = configuration.buildSessionFactory(builder.build());
    UserService userService = new UserService(configuration, builder, factory);

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        String requestUrl = request.getRequestURI();
        String parameter = requestUrl.substring(requestUrl.lastIndexOf("/") + 1);
        int lastParameter = 0;
        if (parameter.equals("all")) {
            lastParameter = 1;
        } else if (StringUtils.isNumeric(parameter)) {
            lastParameter = 2;
        }

        switch (lastParameter) {
            case 1:
                try {
                    listAllUsers(request, response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case 2:
                try {
                    listUserById(request, response, parameter);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            default:
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                break;
        }
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

//        String name = request.getParameter("name");
//        String about = request.getParameter("about");
//        int birthYear = Integer.parseInt(request.getParameter("birthYear"));
//
//        Datastore.getInstance().putPerson(new Person(name, about, birthYear));
    }

    private void listAllUsers(HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        CustomDoublyLinkedList users = userService.getAllUsers();
        if (users != null) {
            response.getOutputStream().print(users.toString());
        } else {
            //That user wasn't found, so return an empty JSON object. We could also return an error.
            response.getOutputStream().println("{}");
        }
    }

    private void listUserById(HttpServletRequest request, HttpServletResponse response, String path) throws Exception {

        User requestedUser = userService.getUserById(Integer.parseInt(path));
        // This is used for generating the JSON response without creating it manually
        if (requestedUser != null) {
            response.getOutputStream().print(requestedUser.toString());
        } else {
            //That user wasn't found, so return an empty JSON object. We could also return an error.
            response.getOutputStream().println("{}");
        }
    }
}