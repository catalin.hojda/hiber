package com.intern.datastructures;

public class CustomDoublyLinkedList {

    private Node head;

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        Node currentNode = head;
        builder.append("[ ");
        while (currentNode.getNext() != null) {
            builder.append(currentNode.getData().toString()).append(", ");
            currentNode = currentNode.getNext();
        }
        builder.append(currentNode.getData().toString()).append(" ]");
        return builder.toString();
    }

    public Node getHead() {
        return head;
    }

    public boolean isEmpty() {
        boolean isEmpty = false;
        if (head == null) {
            isEmpty = true;
        }
        return isEmpty;
    }

    public boolean insertFirst(Object objectToBeInserted) {
        boolean nodeInserted = false;
        if (objectToBeInserted == null) {
            System.out.println("The given object to be inserted first cannot be null");
        } else {
            /**allocate node and put in the data*/
            Node newNode = new Node(objectToBeInserted);
            /**make next of new node as head and previous as NULL*/
            newNode.setNext(head);

            /**change prev of head node to new node*/
            if (head != null) {
                head.setPrev(newNode);
            }
            /**move the head to point to the new node*/
            head = newNode;
            nodeInserted = true;
//            System.out.println(objectToBeInserted + " has been inserted at the beginning of the list");
        }
        return nodeInserted;
    }

    public boolean insertLast(Object objectToBeInserted) {
        boolean nodeInserted = false;

        if (objectToBeInserted == null) {
            System.out.println("The given object to be inserted last cannot be null");
        } else {
            /**allocate node and put in the data*/
            Node newNode = new Node(objectToBeInserted);

            /**copy of the head*/
            Node last = head;

            /**if the list is empty, then make the new node as head*/
            if (head == null) {
                newNode.setPrev(null);
                head = newNode;
                nodeInserted = true;
//                System.out.println(objectToBeInserted + " has been inserted at the end of the list");
            } else {
                /**Else traverse till the last node*/
                while (last.getNext() != null) {
                    last = last.getNext();
                }

                /**change the next of last node*/
                last.setNext(newNode);
                /**make last node as previous of new node*/
                newNode.setPrev(last);
                nodeInserted = true;
//                System.out.println(objectToBeInserted + " has been inserted at the end of the list");
            }
        }
        return nodeInserted;

    }

    public boolean insertAfter(Object objectAfterInsert, Object insertedObject) {
        boolean nodeInserted = false;
        if (objectAfterInsert == null || insertedObject == null) {
            System.out.println("One of the given objects is null");
        } else {

            Node currentNode = head;
            Node newNode = new Node(insertedObject);
            /**if the node after we want to insert is the head*/
            if (currentNode != null && currentNode.getData().equals(objectAfterInsert)) {
                newNode.setNext(currentNode.getNext());
                if (currentNode.getNext() != null) {
                    currentNode.getNext().setPrev(newNode);
                }
                newNode.setPrev(currentNode);
                currentNode.setNext(newNode);
                nodeInserted = true;
//                System.out.println(insertedObject + " has been inserted after " + objectAfterInsert);
            }
            if (!nodeInserted) {
                while (currentNode != null && !currentNode.getData().equals(objectAfterInsert)) {
                    currentNode = currentNode.getNext();
                }
                if (currentNode == null) {
                    System.out.println(objectAfterInsert + " has not been found !");
                } else {
                    newNode.setNext(currentNode.getNext());
                    if (currentNode.getNext() == null) {
                        newNode.setPrev(currentNode);
                        currentNode.setNext(newNode);
                        nodeInserted = true;
//                        System.out.println(insertedObject + " has been inserted after " + objectAfterInsert);
                    } else {
                        currentNode.getNext().setPrev(newNode);
                        newNode.setPrev(currentNode);
                        currentNode.setNext(newNode);
                        nodeInserted = true;
//                        System.out.println(insertedObject + " has been inserted after " + objectAfterInsert);
                    }
                }
            }
        }
        return nodeInserted;

    }

    public void printList() {
        Node currentNode = head;
        System.out.println("LinkedList:");
        while (currentNode != null) {
            System.out.println(currentNode.getData());
            currentNode = currentNode.getNext();
        }
    }

    public Object removeFirst() {
        Object removedObject = null;
        if (head == null) {
            System.out.println("The list is empty");
        } else {
            Node firstNode = head;
            head.getNext().setPrev(null);
            head = head.getNext();
            System.out.println(firstNode.getData() + " has been removed from the beginning of the list");
            removedObject = firstNode.getData();
        }
        return removedObject;
    }

    public Object removeLast() {
        Node copyOfHead = head;
        Object removedObject = null;
        if (head == null) {
            System.out.println("The list is empty");
        } else {
            if (head.getNext() == null) {
                System.out.println(head.getData() + " has been removed from the end of the list");
                head = null;
                removedObject = copyOfHead.getData();
            } else {
                //find the second last node
                while (copyOfHead.getNext().getNext() != null) {
                    copyOfHead = copyOfHead.getNext();
                }
                //iterator now is the second last node
                //make a copy of the last node
                Node lastNode = copyOfHead.getNext();
                //set iterator's next pointer to null which results in losing the last node
                copyOfHead.setNext(null);
                System.out.println(lastNode.getData() + " has been removed from the end of the list");
                removedObject = lastNode.getData();
            }

        }
        return removedObject;
    }

    public Object removeByObject(Object objectToBeRemoved) {
        boolean hasBeenRemoved = false;
        Object removedObject = null;
        if (objectToBeRemoved == null) {
            System.out.println("The given object to be deleted cannot be null");
        } else {

            Node currentNode = head;
            if (currentNode != null && currentNode.getData().equals(objectToBeRemoved)) {
                currentNode.getNext().setPrev(null);
                head = currentNode.getNext();
                System.out.println(objectToBeRemoved + " has been removed from the list");
                removedObject = currentNode.getData();
                hasBeenRemoved = true;
            }
            if (!hasBeenRemoved) {
                while (currentNode != null && !currentNode.getData().equals(objectToBeRemoved)) {
                    currentNode = currentNode.getNext();
                }
                if (currentNode == null) {
                    System.out.println(objectToBeRemoved + " has not been found !");
                } else {
                    removedObject = currentNode.getData();
                    if (currentNode.getNext() == null) {
                        currentNode.getPrev().setNext(null);
                        currentNode.setPrev(null);
                        System.out.println(objectToBeRemoved + " has been removed from the list");
                    } else {
                        currentNode.getPrev().setNext(currentNode.getNext());
                        currentNode.getNext().setPrev(currentNode.getPrev());
                        System.out.println(objectToBeRemoved + " has been removed from the list");
                    }
                }
            }
        }
        return removedObject;
    }

}