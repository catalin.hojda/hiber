package com.intern.datastructures;

import com.intern.model.User;

public class CustomQueue {
    private static final int arrayLength = 100;
    private User[] queueOfUsers = new User[arrayLength];
    private int queueIndex;

    public boolean isEmpty() {
        return queueIndex == 0;
    }

    public CustomQueue() {
        this.queueIndex = 0;
    }

    public boolean add(User user) {
        if (queueIndex == arrayLength - 1) {
            System.out.println("The queue is full");
            return false;
        } else {
            queueOfUsers[queueIndex] = user;
            queueIndex++;
            System.out.println("User " + user.getName() + " added into queue");
            return true;
        }
    }

    public User removeUser() {
        if (isEmpty()) {
            System.out.println("The queue is empty");
            return null;
        } else {
            User user = this.queueOfUsers[0];
            System.arraycopy(queueOfUsers, 1, queueOfUsers, 0, arrayLength - 1);
            this.queueIndex--;
            System.out.println("User " + user.getName() + " removed from queue");
            return user;
        }
    }
}

