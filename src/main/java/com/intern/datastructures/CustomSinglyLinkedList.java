package com.intern.datastructures;


public class CustomSinglyLinkedList {
    private Node head;

    static class Node {
        private Object data;
        private Node next;

        public Node(Object data) {
            this.data = data;
            next = null;
        }
    }

    /**
     * Inserts a new Node at the beginning of the list
     */
    public boolean insertFirst(Object objectToBeInserted) {
        boolean nodeInserted = false;
        if (objectToBeInserted == null) {
            System.out.println("The given object to be inserted first cannot be null");
        } else {
            //Allocate the Node and put in the data(User)
            Node newNode = new Node(objectToBeInserted);
            //Make next of new Node as head
            newNode.next = head;
            //Move the head to point to new Node
            head = newNode;
            //newNode = head;
            nodeInserted = true;
//            User userToBeInserted = null;
//            if (objectToBeInserted instanceof User){
//                userToBeInserted = (User) objectToBeInserted;
//            }
//            System.out.println(objectToBeInserted.getName() + " has been inserted at the beginning of the list");
            System.out.println("Object " + objectToBeInserted + " has been inserted at the beginning of the list");
        }
        return nodeInserted;
    }

    /**
     * Inserts a new node after the given node as parameter
     */
    public boolean insertAfter(Object objectAfterInsert, Object insertedObject) {
        boolean nodeInserted = false;
        if (objectAfterInsert == null || insertedObject == null) {
            System.out.println("One of the given objects is null");
        } else {
            Node nodeBeforeWeInsert = head;
            Node nodeAfterWeInsert = head;
            Node newNode = new Node(insertedObject);
            if (nodeAfterWeInsert != null && nodeAfterWeInsert.data.equals(objectAfterInsert)) {
                newNode.next = nodeAfterWeInsert.next;
                nodeAfterWeInsert.next = newNode;
//                System.out.println(insertedObject.getName() + " has been inserted after " + objectAfterInsert.getName());
                System.out.println("Object " + insertedObject + " has been inserted after " + objectAfterInsert);
                nodeInserted = true;
            }
            if (!nodeInserted) {
                while (nodeBeforeWeInsert != null && !nodeAfterWeInsert.data.equals(objectAfterInsert)) {
                    nodeAfterWeInsert = nodeBeforeWeInsert;
                    nodeBeforeWeInsert = nodeBeforeWeInsert.next;
                }
                if (nodeBeforeWeInsert == null && !nodeAfterWeInsert.data.equals(objectAfterInsert)) {
//                    System.out.println(objectAfterInsert.getName() + " has not been found !");
                    System.out.println(objectAfterInsert + " has not been found");
                } else {
                    if (nodeAfterWeInsert.next == null) {
                        newNode.next = null;
                        nodeAfterWeInsert.next = newNode;
                        nodeInserted = true;
                        System.out.println(insertedObject + " has been inserted after " + objectAfterInsert);
                    } else {
                        newNode.next = nodeBeforeWeInsert;
                        nodeAfterWeInsert.next = newNode;
                        nodeInserted = true;
                        System.out.println(insertedObject + " has been inserted after " + objectAfterInsert);
                    }
                }
            }
        }
        return nodeInserted;
    }

    /**
     * Inserts a new node at the end of the list
     */
    public boolean insertLast(Object objectToBeInserted) {
        boolean nodeInserted = false;
        if (objectToBeInserted == null) {
            System.out.println("The given object to be inserted last cannot be null");
        } else {
            Node newNode = new Node(objectToBeInserted);
            if (head == null) {
                head = newNode;
                nodeInserted = true;
                System.out.println(objectToBeInserted + " has been inserted at the end of the list");
            } else {
                Node last = head;
                while (last.next != null) {
                    last = last.next;
                }
                last.next = newNode;
                nodeInserted = true;
                System.out.println(objectToBeInserted + " has been inserted at the end of the list");
            }
        }
        return nodeInserted;
    }

    public void printList() {
        Node currentNode = head;
        System.out.println("LinkedList:");
        while (currentNode != null) {
            System.out.println(currentNode.data);
            currentNode = currentNode.next;

        }
    }

    public Object removeFirst() {
        Object removedObject = null;
        if (head == null) {
            System.out.println("The list is empty");
        } else {
            Node firstNode = head;
            head = head.next;
            System.out.println(firstNode.data + " has been removed from the beginning of the list");
            removedObject = firstNode.data;
        }
        return removedObject;
    }

    public Object removeLast() {
        Object removedObject = null;
        Node copyOfHead = head;
        if (head == null) {
            System.out.println("The list is empty");
        } else {
            if (head.next == null) {
                System.out.println(head.data + " has been removed from the end of the list");
                head = null;
                removedObject = copyOfHead.data;
            } else {
                //find the second last node
                while (copyOfHead.next.next != null) {
                    copyOfHead = copyOfHead.next;
                }
                //iterator now is the second last node
                //make a copyOfHead of the last node
                Node lastNode = copyOfHead.next;
                //set iterator's next pointer to null which results in losing the last node
                copyOfHead.next = null;
                removedObject = lastNode.data;
                System.out.println(lastNode.data + " has been removed from the end of the list");
            }

        }
        return removedObject;
    }

    public Object removeByUser(Object objectToBeRemoved) {
        boolean hasBeenRemoved = false;
        Object removedObject = null;
        if (objectToBeRemoved == null) {
            System.out.println("The given object to be deleted cannot be null");
        } else {
            Node currentNode = head;
            Node previousNode = null;
            if (currentNode != null && currentNode.data.equals(objectToBeRemoved)) {
                removedObject = currentNode.data;
                hasBeenRemoved = true;
                head = currentNode.next;
                System.out.println(objectToBeRemoved + " has been removed from the list");
            }
            if (!hasBeenRemoved) {
                while (currentNode != null && !currentNode.data.equals(objectToBeRemoved)) {
                    previousNode = currentNode;
                    currentNode = currentNode.next;
                }
                if (currentNode == null) {
                    System.out.println(objectToBeRemoved + " has not been found !");
                } else {
                    assert previousNode != null;
                    previousNode.next = currentNode.next;
                    removedObject = currentNode.data;
                    System.out.println(currentNode.data + " has been removed from the list");
                }

            }

        }
        return removedObject;
    }

}