package com.intern.datastructures;

public class Node {
    private Object data;
    private Node prev;
    private Node next;

    Node(Object object) {
        this.data = object;
    }

    public Object getData() {
        return data;
    }

    public Node getPrev() {
        return prev;
    }

    public Node getNext() {
        return next;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public void setPrev(Node prev) {
        this.prev = prev;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    @Override
    public String toString() {
        return data.toString();
    }
}
