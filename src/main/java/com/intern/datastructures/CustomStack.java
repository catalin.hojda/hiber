package com.intern.datastructures;

import com.intern.model.User;

public class CustomStack {
    private static final int arrayLength = 100;
    private int stackHead;
    private User[] stackOfUsers = new User[arrayLength];

    public CustomStack() {
        this.stackHead = -1;
    }

    public boolean isEmpty() {
        return stackHead < 0;
    }

    public boolean push(User user) {
        if (stackHead >= (arrayLength - 1)) {
            System.out.println("Stack Overflow");
            return false;
        } else {
            stackHead++;
            stackOfUsers[stackHead] = user;
            System.out.println("User " + user.getName() + " pushed into stack");
            return true;
        }
    }

    public User pop() {
        User userFromTheStackHead = null;
        if (stackHead < 0) {
            System.out.println("Stack is empty");
            return null;
        } else {
            userFromTheStackHead = stackOfUsers[stackHead];
            stackHead--;
            System.out.println("User " + userFromTheStackHead.getName() + " popped from stack");
            return userFromTheStackHead;
        }

    }

    public User peek() {
        User userFromTheStackHead = null;
        if (stackHead < 0) {
            System.out.println("Stack is empty");
            return null;
        } else {
            userFromTheStackHead = stackOfUsers[stackHead];
            System.out.println("The user at the top of the stack is: " + userFromTheStackHead.getName());
            return userFromTheStackHead;
        }

    }


}
