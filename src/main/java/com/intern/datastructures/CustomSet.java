package com.intern.datastructures;

import java.util.ArrayList;

public class CustomSet {
    private final ArrayList<Object> arrayList = new ArrayList<>();


    public boolean add(Object insertedObject) {
        boolean hasBeenInserted = false;
        if (insertedObject == null) {
            System.out.println("Object to be added cannot be null ");
        } else {
            if (!arrayList.contains(insertedObject)) {
                arrayList.add(insertedObject);
                hasBeenInserted = true;
                System.out.println("Inserted : " + insertedObject);
            }
        }
        return hasBeenInserted;
    }

    public boolean remove(Object removedObject) {
        boolean hasBeenRemoved = false;
        if (removedObject == null) {
            System.out.println("Object to be deleted cannot be null");
        } else {
            if (!arrayList.isEmpty()) {
                if (arrayList.contains(removedObject)) {
                    for (Object objectIterator : arrayList) {
                        if (removedObject.equals(objectIterator)) {
                            arrayList.remove(removedObject);
                            hasBeenRemoved = true;
                            System.out.println("Deleted : " + removedObject);
                            break;
                        }
                    }
                } else {
                    System.out.println("Object does not exist");
                }
            } else {
                System.out.println("Set is empty");
            }
        }
        return hasBeenRemoved;
    }

    public void removeAll() {
        arrayList.clear();
    }

    public void print() {
        if (arrayList.isEmpty()) {
            System.out.println("Set is empty");
        } else {
            for (Object objectIterator : arrayList) {
                System.out.println(objectIterator);
            }
        }
    }

}
