package com.intern.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.Gson;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@NamedQueries(
        {
                @NamedQuery(name = "User_getEmployeeUsers", query = "from User where role = :userRole"),
                @NamedQuery(name = "User_getUserByEmail", query = "from User where email = :userEmail")
        }
)
@Entity
@Table(name = "users", uniqueConstraints = {
        @UniqueConstraint(columnNames = "id"),
        @UniqueConstraint(columnNames = "email"),
        //@UniqueConstraint(columnNames = "phone"),
        @UniqueConstraint(columnNames = "cnp")})
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String email;
    private String name;
    private String phone;
    private String cnp;


    @Temporal(TemporalType.DATE)
    @Column(name = "employment_date")
    private Date employment_date;

    private String role;
    private int manager_id;
    private int days_per_year;
    private int current_year_remaining_days;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER,
            mappedBy = "user", orphanRemoval = true)
    private List<Holiday> holidaysList = new ArrayList<Holiday>();

    public User() {
    }

    public User(String email, String name, String phone, String cnp, Date employment_date, String role, int manager_id, int days_per_year, int current_year_remaining_days) {
        this.email = email;
        this.name = name;
        this.phone = phone;
        this.cnp = cnp;
        this.employment_date = employment_date;
        this.role = role;
        this.manager_id = manager_id;
        this.days_per_year = days_per_year;
        this.current_year_remaining_days = current_year_remaining_days;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getEmployment_date() {
        return employment_date;
    }

    public void setEmployment_date(Date employment_date) {
        this.employment_date = employment_date;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public int getManager_id() {
        return manager_id;
    }

    public void setManager_id(int manager_id) {
        this.manager_id = manager_id;
    }

    public int getDays_per_year() {
        return days_per_year;
    }

    public void setDays_per_year(int days_per_year) {
        this.days_per_year = days_per_year;
    }

    public int getCurrent_year_remaining_days() {
        return current_year_remaining_days;
    }

    public void setCurrent_year_remaining_days(int current_year_remaining_days) {
        this.current_year_remaining_days = current_year_remaining_days;
    }

    public String getCnp() {
        return cnp;
    }

    public void setCnp(String cnp) {
        this.cnp = cnp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(email, user.email) &&
                Objects.equals(name, user.name) &&
                Objects.equals(cnp, user.cnp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, name, cnp);
    }

    public List<Holiday> getHolidaysList() {
        return holidaysList;
    }

    public void setHolidaysList(List<Holiday> holidaysList) {
        this.holidaysList = holidaysList;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}