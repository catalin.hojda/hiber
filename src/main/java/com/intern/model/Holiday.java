package com.intern.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;

@NamedQueries(
        {
                @NamedQuery(name = "Holiday_getHolidaysByUser", query = "from Holiday where requester_id = :userId")
        }
)
@Entity
@Table(name = "holidays")
public class Holiday {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private int requester_id;

    @Temporal(TemporalType.DATE)
    private Date start_date;

    @Temporal(TemporalType.DATE)
    private Date end_date;

    private String type;
    private String status;

    @Temporal(TemporalType.DATE)
    private Date approved_time;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "requester_id", updatable = false, insertable = false)
    private User user;

    public Holiday() {
    }

    public Holiday(int requester_id, Date start_date, Date end_date, String type, String status, Date approved_time) {
        this.requester_id = requester_id;
        this.start_date = start_date;
        this.end_date = end_date;
        this.type = type;
        this.status = status;
        this.approved_time = approved_time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRequester_id() {
        return requester_id;
    }

    public void setRequester_id(int requester_id) {
        this.requester_id = requester_id;
    }

    public Date getStart_date() {
        return start_date;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public Date getEnd_date() {
        return end_date;
    }

    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getApproved_time() {
        return approved_time;
    }

    public void setApproved_time(Date approved_time) {
        this.approved_time = approved_time;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Holiday{" +
                "id=" + id +
                ", requester_id=" + requester_id +
                ", start_date=" + start_date +
                ", end_date=" + end_date +
                ", type='" + type + '\'' +
                ", status='" + status + '\'' +
                ", approved_time=" + approved_time +
                ", user=" + user.getEmail() +
                '}';
    }
}

