package com.intern.model;

import com.google.gson.Gson;

import javax.persistence.*;
import java.util.Date;

@NamedQueries(
        {
                @NamedQuery(name = "AuditLog_getLogsByUserEmail", query = "from AuditLog where initiator_email = :userEmail")
        }
)

@Entity
@Table(name = "auditlog")
public class AuditLog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String action_type;

    @Temporal(TemporalType.DATE)
    @Column(name = "action_time")
    private Date action_time;

    private String initiator_email;

    public AuditLog() {
    }

    public AuditLog(String action_type, Date action_time, String initiator_email) {
        this.action_type = action_type;
        this.action_time = action_time;
        this.initiator_email = initiator_email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAction_type() {
        return action_type;
    }

    public void setAction_type(String action_type) {
        this.action_type = action_type;
    }

    public Date getAction_time() {
        return action_time;
    }

    public void setAction_time(Date action_time) {
        this.action_time = action_time;
    }

    public String getInitiator_email() {
        return initiator_email;
    }

    public void setInitiator_email(String initiator_email) {
        this.initiator_email = initiator_email;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}