package com.intern.model;

import javax.persistence.*;
import java.util.Date;

@NamedQueries(
        @NamedQuery(name = "PublicHoliday_getPublicHolidaysAfterDate", query = "from PublicHoliday where day > :startDay")
)

@Entity
@Table(name = "publicholidays")
public class PublicHoliday {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Temporal(TemporalType.DATE)
    private Date day;

    private String description;

    public PublicHoliday() {
    }

    public PublicHoliday(Date day, String description) {
        this.day = day;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDay() {
        return day;
    }

    public void setDay(Date day) {
        this.day = day;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "PublicHoliday{" +
                "id=" + id +
                ", day=" + day +
                ", description='" + description + '\'' +
                '}';
    }
}

